<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','ArticleController@index')->middleware(['auth','logger']);

Auth::routes();

Route::get('/', 'ArticleController@index')->middleware(['auth','logger']);
Route::get('/article','ArticleController@create')->middleware(['auth','logger']);
Route::post('/article','ArticleController@store')->middleware(['auth','logger']);
Route::get('/article/{id}','ArticleController@show')->middleware(['auth','logger']);
Route::put('/article/{id}','ArticleController@update')->middleware(['auth','logger']);
Route::delete('/article/{id}','ArticleController@delete')->middleware(['auth','logger']);
Route::get('/log', 'HomeController@log')->middleware(['auth','logger']);
