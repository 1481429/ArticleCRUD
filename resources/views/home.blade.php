@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Liste des articles</div>

                <div class="panel-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Titre</th>
                            <th>Auteur</th>
                            <th>      </th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($articles))
                        @foreach($articles as $article)
                            <tr>
                                <td>{{$article->titre}}</td>
                                <td>{{\App\User::find($article->auteur_id)->name}}</td>
                                <td><a href="/article/{{$article->id}}" class="btn btn-info">Voir article</a></td>
                            </tr>
                        @endforeach
                        @endif

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
