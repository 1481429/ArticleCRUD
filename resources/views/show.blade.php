@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div align="center" class="panel panel-default">
                    <form method="post">
                        <div class="panel-heading">
                            <h3 class="panel-title" id="paragTitre">{{$article->titre}} -  {{\App\User::find($article->auteur_id)->name}}</h3>
                            <input type="text" id="inputTitre" name="titre" class="form-control" value="{{$article->titre}}">
                        </div>
                        <div class="panel-body">
                            <p id="paragContenu">{{$article->contenu}}</p>
                            <textarea rows="4" cols="50" id="inputContenu" type="text" name="contenu"  id="inputDesc" name="Description" class="form-control" value="{{$article->contenu}}" >{{$article->contenu}}</textarea>
                        </div>
                        <button id="btnSauve" class="btn btn-primary">Sauvegarder L'article</button>
                    {{method_field('put')}}
                        {{csrf_field()}}
                    </form>
                    <div style="padding-bottom: 10px">
                    <a id="btnModif" href="javascript:;" onclick="ModifierInfo()" class="btn btn-warning" role="button">Modifier L'article</a>
                    <form method="post" style="display:inline">
                        {{csrf_field()}}
                        {{method_field('delete')}}
                        <button  class="btn btn-danger">Supprimer l'article</button>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<script>
    window.onload=Onload;
    function Onload()
    {
        $('#inputTitre').hide();
        $('#inputContenu').hide();
        $('#btnSauve').hide();
    }
    function ModifierInfo()
    {   $('#paragTitre').hide();
        $('#paragContenu').hide();
        $('#inputTitre').show();
        $('#inputContenu').show();
        $('#btnSauve').show();
        $('#btnModif').hide();
    }

</script>