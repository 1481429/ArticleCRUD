
@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4">

    <form method="post">
        {{csrf_field()}}
        <div class="form-group">
            <label for="txtNom">Nom de l'article</label>
            <input type="text" id="txtNom" name="titre" class="form-control" value="{{old('txtNom')}}" placeholder="">
        </div>

        <div class="form-group">
            <label for="txtDescription">Contenu</label>
            <textarea id="txtDescription" name="contenu" class="form-control" value="{{old('txtDescription')}}"></textarea>
        </div>
        <div class="form-group">
        <button style="display: block; margin: 0 auto;" type="submit" class="btn btn-primary">Créer l'article</button>
        </div>
    </form>
    </div>
    <div class="col-md-4"></div>
</div>
@endsection
