@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Liste des requêtes</div>
                        <div class="panel-body">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>URL</th>
                                    <th>Methode</th>
                                    <th>Courriel</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(isset($logs))
                                    @foreach($logs as $log)
                                        <tr>
                                            <td>{{$log->url}}</td>
                                            <td>{{$log->methode}}</td>
                                            <td>{{$log->User->email}}</td>
                                            <td>{{$log->created_at}}</td>
                                        </tr>
                                    @endforeach
                                @endif
                                <tr>
                                    <td>/article</td>
                                    <td>GET</td>
                                    <td>fraud@fraud.com</td>
                                    <td>2016-11-09 22:50:52</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection