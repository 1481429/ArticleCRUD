<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Log as Log;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    public  function log()
    {
        $logs = Log::all();
        return view('logs',['logs'=> $logs]);
    }
}
