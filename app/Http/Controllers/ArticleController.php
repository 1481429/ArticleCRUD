<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article as Article;
use Carbon\Carbon;
use App\Log as Log;
use Illuminate\Support\Facades\Mail;
use App\Mail\ArticleVu;
use Auth;

class ArticleController extends Controller
{
    public function create()
    {
        return view('create');
    }
    public function store(Request $request)
    {

        $article = new Article();
        $article->titre = $request->titre;
        $article->contenu = $request->contenu;
        $article->auteur_id = Auth::user()->id;
        $article->save();
        return redirect('/');

    }
    public function delete(Request $request,$id)
    {
        Article::find($id)->delete();
        return redirect('/');
    }
    public function index()
    {
        $articles = Article::all();

        return view('home',['articles'=>$articles]);
    }

    public function update(Request $request,$id)
    {
        $article =  Article::find($id);
        $article->titre = $request->titre;
        $article->contenu = $request->contenu;
        $article->save();
        return redirect('/');
    }
    public function show($id)
    {


        $article =  Article::find($id);

        Mail::to(\App\User::find($article->auteur_id)->email)->send(new ArticleVu($article->titre,$article->id,Auth::user()->name,Carbon::now()));

        return view('show',['article'=>$article]);
    }
    //
}
