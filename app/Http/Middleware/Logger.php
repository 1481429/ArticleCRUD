<?php

namespace App\Http\Middleware;

use App\Log as Log;
use Auth;
use Closure;

class Logger
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $log = new Log();
        $log->url = $request->url();
        $log->methode = $request->method();
        $log->user_id=Auth::User()->id;
        $log->save();
        return $next($request);
    }
}
