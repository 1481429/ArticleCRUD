<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ArticleVu extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $TitreArticle;
    public $NumeroArticle;
    public $NomVisionneur;
    public $DateHeure;
    public function __construct($Titre,$Numero,$Nom,$DH)
    {
        //
        $this->TitreArticle = $Titre;
        $this->NumeroArticle = $Numero;
        $this->NomVisionneur = $Nom;
        $this->DateHeure = $DH;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $format = 'Nouvelle lecture de votre article %s (No. %s)';

        return $this->from('testmail@mailinator.com')
            ->subject(sprintf($format,$this->TitreArticle,$this->NumeroArticle))
            ->view('VArticleVu');
    }
}
